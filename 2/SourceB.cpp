#include <iostream>
#include "sqlite3.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <exception>
 
using namespace std;


void clearTable();
void printTable();
int callback(void* notUsed, int argc, char** argv, char** colName);

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);


unordered_map<string, vector<string>> results;

int main()
{
	int rc;
	sqlite3* db;
	char* zErrMsg = 0;
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	clearTable();
	//Part A
	//carPurchase(-4, 8, db, zErrMsg);
	//carPurchase(12, 42, db, zErrMsg);
	//carPurchase(5, 15, db, zErrMsg);
	//Part B
	balanceTransfer(1, 5, 4000, db, zErrMsg);
	balanceTransfer(1, 5, 9000000, db, zErrMsg);
	balanceTransfer(-5, 123, 4000, db, zErrMsg);
	system("pause");
	return 0;
}


//-----------------FUNCTIONS-----------------//

/*
checks to see if a buyer can by a car
input: id of buyer, id of car, data base , error pointer
output: true if buyer can buy car, false if not
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	int buyerBalance = 0;
	int carPrice = 0;
	int available = 0;
	string msg;
	clearTable();

	//--finding the buyer's balance and the car price
	//balance
	rc = sqlite3_exec(db,"SELECT buyer_id,balance FROM accounts",callback,0,&zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}
	
	//corner cases
	if (buyerid <= results.find("balance")->second.size() && buyerid > 0)
	{
		buyerBalance = std::atoi(results.find("balance")->second.at(buyerid - 1).c_str());
		clearTable();
	}
	else //error
	{
		cout << "buyer ID is not in the data base\n";
		return false;
	}
	

	//car price
	rc = sqlite3_exec(db, "SELECT price,available FROM cars", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}


	if (carid <= results.find("price")->second.size() && carid > 0)
	{
		carPrice = std::atoi(results.find("price")->second.at(carid - 1).c_str());
		available = std::atoi(results.find("available")->second.at(carid - 1).c_str());
		clearTable();
	}
	else //error
	{
		cout << "car ID is not in the data base\n";
		return false;
	}

	if (carPrice <= buyerBalance && available)
	{
		//--buy the car
		msg = "BEGIN TRANSACTION";
		rc = sqlite3_exec(db, msg.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			exit(1);
		}

		//set balance
		msg = "UPDATE accounts SET balance=";
		msg += to_string(buyerBalance - carPrice);
		msg += " WHERE buyer_id=";
		msg += to_string(buyerid);

		rc = sqlite3_exec(db, msg.c_str(), NULL, 0, &zErrMsg);
		
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			exit(1);
		}
		//set available
		msg = "UPDATE cars SET available=0 WHERE id=";
		msg += to_string(carid);

		rc = sqlite3_exec(db, msg.c_str(), NULL, 0, &zErrMsg);
		
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			exit(1);
		}

		rc = sqlite3_exec(db,"COMMIT", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			exit(1);
		}
		return true;
	}
	return false;
}

/*
transfers money from one account to another one
input: id from, id to, amount to transfer, data base, error buffer
output: true if transfered successfuly, false if not.
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string msg; 
	int tableSize = 0;
	int fromBalance = 0;
	int toBalance = 0;

	//checking if transfer can happen
	msg = "SELECT * FROM accounts";
	clearTable();
	rc = sqlite3_exec(db, msg.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}
	

	//checking for egde cases
	if (amount < 0)
	{
		return false;
	}
	
	tableSize = results.find("id")->second.size();
	
	if (from > tableSize || from <= 0 || to > tableSize || to <= 0)
	{
		return false;
	}

	fromBalance = atoi(results.find("balance")->second[from - 1].c_str());
	toBalance = atoi(results.find("balance")->second[to - 1].c_str());
	
	if (amount > fromBalance)
	{
		return false;
	}

	//transferring
	msg = "BEGIN TRANSACTION";
	rc = sqlite3_exec(db, msg.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}

	msg = "UPDATE accounts SET balance=";
	msg += to_string(fromBalance - amount);
	msg += " WHERE id=";
	msg += to_string(from);

	rc = sqlite3_exec(db, msg.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}

	msg = "UPDATE accounts SET balance=";
	msg += to_string(toBalance + amount);
	msg += " WHERE id=";
	msg += to_string(to);
	
	rc = sqlite3_exec(db, msg.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}

	rc = sqlite3_exec(db, "COMMIT", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		exit(1);
	}

	return true;
}

//clears results
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

//prints results
void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

//enters table from db to results
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}
