#include <iostream>
#include "sqlite3.h"

using std::cout;
using std::endl;

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;


	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	//creting a table
	rc = sqlite3_exec(db, "CREATE TABLE people (id integer primary key autoincrement,name TEXT)", NULL, 0, &zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	//inserting data 
	rc = sqlite3_exec(db, "INSERT INTO people(name) VALUES('Eli')", NULL, 0, &zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "INSERT INTO people(name) VALUES('Bob')", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "INSERT INTO people(name) VALUES('pupper')", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	rc = sqlite3_exec(db, "UPDATE people SET name='doggo' where id=3", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	return 0;
}